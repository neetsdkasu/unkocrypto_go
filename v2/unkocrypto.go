// unkocrypto
// author: Leonardone @ NEETSDKASU
// MIT License

package unkocrypto_go

import (
	"encoding/binary"
	"hash"
	"io"
	"log"
)

const (
	MinBlockSize = 32
	MaxBlockSize = 1 << 20
	// MetaSize = unsafe.Sizeof(int32(0)) + unsafe.Sizeof(int64(0))
	MetaSize  = 4 + 8
	byteBound = 1 << 8 // 256
)

type IntRNG interface {
	NextInt() int32
}

type Cause int

const (
	InvalidBlockSize Cause = iota
	InvalidSourceSize
	InvalidDataCount
	InvalidData
	InvalidChecksum
)

type UnkocryptoError struct {
	Cause
}

func (self UnkocryptoError) Error() string {
	return self.String()
}

func (self UnkocryptoError) String() string {
	return "UnkocryptoError(" + self.Cause.String() + ")"
}

func (self Cause) String() string {
	switch self {
	case InvalidBlockSize:
		return "InvalidBlockSize"
	case InvalidSourceSize:
		return "InvalidSourceSize"
	case InvalidDataCount:
		return "InvalidDataCount"
	case InvalidData:
		return "InvalidData"
	case InvalidChecksum:
		return "InvalidChecksum"
	default:
		log.Panicln("Invalid Cause", int(self))
		return ""
	}
}

type byteReader struct {
	io.Reader
}

func getByteReader(reader io.Reader) io.ByteReader {
	if r, ok := reader.(io.ByteReader); ok {
		return r
	} else {
		return &byteReader{reader}
	}
}

func (self *byteReader) ReadByte() (byte, error) {
	var buf [1]byte
	_, err := io.ReadFull(self.Reader, buf[:])
	return buf[0], err
}

type byteWriter struct {
	io.Writer
}

func getByteWriter(writer io.Writer) io.ByteWriter {
	if w, ok := writer.(io.ByteWriter); ok {
		return w
	} else {
		return &byteWriter{writer}
	}
}

func (self *byteWriter) WriteByte(b byte) error {
	var buf [1]byte
	buf[0] = b
	_, err := self.Writer.Write(buf[:])
	return err
}

type hasher interface {
	hash.Hash
	io.ByteWriter
	getValue() int64
}

type hasher32 struct {
	hash.Hash32
	io.ByteWriter
}

type hasher64 struct {
	hash.Hash64
	io.ByteWriter
}

type hasherX struct {
	hash.Hash
	io.ByteWriter
	buf []byte
}

func (self *hasher32) getValue() int64 {
	return int64(self.Sum32())
}

func (self *hasher64) getValue() int64 {
	return int64(self.Sum64())
}

func (self *hasherX) getValue() int64 {
	self.Sum(self.buf)
	switch self.Size() {
	case 1:
		return int64(self.buf[0])
	case 2, 3:
		return int64(binary.BigEndian.Uint16(self.buf))
	case 4, 5, 6, 7:
		return int64(binary.BigEndian.Uint32(self.buf))
	default:
		return int64(binary.BigEndian.Uint64(self.buf))
	}
}

func getHasher(h hash.Hash) hasher {
	if h64, ok := h.(hash.Hash64); ok {
		return &hasher64{h64, getByteWriter(h64)}
	} else if h32, ok := h.(hash.Hash32); ok {
		return &hasher32{h32, getByteWriter(h32)}
	} else {
		return &hasherX{h, getByteWriter(h), make([]byte, h.Size())}
	}
}

func nextInt(rng IntRNG, bound int32) int32 {
	if (bound & -bound) == bound {
		return int32((int64(bound) * int64(uint32(rng.NextInt())>>1)) >> 31)
	}
	for {
		bits := int32(uint32(rng.NextInt()) >> 1)
		val := bits % bound
		if bits-val+(bound-1) >= 0 {
			return val
		}
	}
}

func Decrypt(
	blockSize int,
	checksum hash.Hash,
	rng IntRNG,
	src io.Reader,
	dst io.Writer,
) (length uint64, err error) {
	if blockSize < MinBlockSize || MaxBlockSize < blockSize {
		err = UnkocryptoError{InvalidBlockSize}
		return
	}
	var dataSize int = blockSize - MetaSize
	var mask = make([]byte, blockSize)
	var indexes = make([]int, blockSize)
	var data = make([]byte, blockSize)
	var oneByte byte
	var reader = getByteReader(src)
	var writer = getByteWriter(dst)
	var checkSum = getHasher(checksum)
	if oneByte, err = reader.ReadByte(); err != nil {
		if err == io.EOF {
			err = UnkocryptoError{InvalidSourceSize}
		}
		return
	}
	var isEOF = false
	for !isEOF {
		for i := 0; i < blockSize; i++ {
			mask[i] = byte(nextInt(rng, byteBound))
			indexes[i] = i
		}
		for i := 0; i < blockSize; i++ {
			var j = int(nextInt(rng, int32(blockSize-i))) + i
			indexes[i], indexes[j] = indexes[j], indexes[i]
		}
		for _, j := range indexes {
			if isEOF {
				err = UnkocryptoError{InvalidSourceSize}
				return
			}
			data[j] = oneByte ^ mask[j]
			if oneByte, err = reader.ReadByte(); err != nil {
				if err == io.EOF {
					isEOF = true
				} else {
					return
				}
			}
		}
		var count = int32(binary.BigEndian.Uint32(data[dataSize:]))
		var code = int64(binary.BigEndian.Uint64(data[dataSize+4:]))
		if count < 0 || (count == 0 && err != io.EOF) || dataSize < int(count) {
			err = UnkocryptoError{InvalidDataCount}
			return
		}
		length += uint64(count)
		checkSum.Reset()
		for i, d := range data[:dataSize] {
			if i < int(count) {
				if err = writer.WriteByte(d); err != nil {
					return
				}
				if err = checkSum.WriteByte(d); err != nil {
					return
				}
			} else if d != 0 {
				err = UnkocryptoError{InvalidData}
				return
			}
		}
		if code != checkSum.getValue() {
			err = UnkocryptoError{InvalidChecksum}
			return
		}
	}

	err = nil
	return
}

func Encrypt(
	blockSize int,
	checksum hash.Hash,
	rng IntRNG,
	src io.Reader,
	dst io.Writer,
) (length uint64, err error) {
	if blockSize < MinBlockSize || MaxBlockSize < blockSize {
		err = UnkocryptoError{InvalidBlockSize}
		return
	}
	var dataSize int = blockSize - MetaSize
	var data = make([]byte, blockSize)
	var reader = getByteReader(src)
	var checkSum = getHasher(checksum)
	var oneByte byte
	var isEOF = false
	if oneByte, err = reader.ReadByte(); err != nil {
		if err == io.EOF {
			isEOF = true
		} else {
			return
		}
	}
	for {
		length += uint64(blockSize)
		checkSum.Reset()
		var count int = 0
		for ; count < dataSize; count++ {
			if isEOF {
				break
			}
			if err = checkSum.WriteByte(oneByte); err != nil {
				return
			}
			data[count] = oneByte ^ byte(nextInt(rng, byteBound))
			if oneByte, err = reader.ReadByte(); err != nil {
				if err == io.EOF {
					isEOF = true
				} else {
					return
				}
			}
		}
		for i := count; i < dataSize; i++ {
			data[i] = byte(nextInt(rng, byteBound))
		}
		binary.BigEndian.PutUint32(data[dataSize:], uint32(count))
		binary.BigEndian.PutUint64(data[dataSize+4:], uint64(checkSum.getValue()))
		for i := dataSize; i < len(data); i++ {
			data[i] ^= byte(nextInt(rng, byteBound))
		}
		for i := 0; i < len(data); i++ {
			var j = int(nextInt(rng, int32(len(data)-i))) + i
			data[i], data[j] = data[j], data[i]
		}
		if _, err = dst.Write(data); err != nil {
			return
		}
		if isEOF {
			break
		}
	}
	err = nil
	return
}
