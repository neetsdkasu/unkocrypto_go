package unkocrypto_go

import (
	"bytes"
	"hash/crc32"
	"math/rand"
	"testing"
)

type javaRandomLikeSource struct {
	seed int64
}

func newJavaRandomLikeSource(seed int64) *javaRandomLikeSource {
	src := new(javaRandomLikeSource)
	src.Seed(seed)
	return src
}

func (self *javaRandomLikeSource) Int63() int64 {
	self.seed = (self.seed*0x5DEECE66D + 0xB) & ((1 << 48) - 1)
	value := self.seed >> (48 - 32)
	// rand.Rand.Uint32() はInt63()の上位ビットのみ使う (下位のビットは使わないので無考慮)
	return value << 31
}

func (self *javaRandomLikeSource) Seed(seed int64) {
	self.seed = (seed ^ 0x5DEECE66D) & ((1 << 48) - 1)
}

type javaRandom struct {
	seed int64
}

func newJavaRandom(seed int64) *javaRandom {
	rng := new(javaRandom)
	rng.setSeed(seed)
	return rng
}

func (self *javaRandom) setSeed(seed int64) {
	self.seed = (seed ^ 0x5DEECE66D) & ((1 << 48) - 1)
}

func (self *javaRandom) next(bits int) uint32 {
	self.seed = (self.seed*0x5DEECE66D + 0xB) & ((1 << 48) - 1)
	return uint32(self.seed >> (48 - bits))
}

// implement IntRNG interface
func (self *javaRandom) NextInt() int32 {
	return int32(self.next(32))
}

type RandWrapper struct {
	r *rand.Rand
}

func (self *RandWrapper) NextInt() int32 {
	return int32(self.r.Uint32())
}

func TestDecryptWithRand(t *testing.T) {

	var blockSize int = MinBlockSize

	var seed int64 = 123456789

	var data_src = []uint8{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}

	var secret_src = []uint8{
		0xa2, 0xfc, 0x45, 0x90, 0x64, 0x80, 0x77, 0x46, 0x3f, 0x7e,
		0x1d, 0x7c, 0x64, 0xfe, 0x5c, 0x98, 0x7a, 0x00, 0x79, 0xa8,
		0x64, 0xf2, 0x7d, 0xc1, 0xe3, 0x66, 0x31, 0x31, 0x1e, 0x62,
		0xb6, 0x04,
	}

	var rng = &RandWrapper{rand.New(newJavaRandomLikeSource(seed))}

	var crc = crc32.NewIEEE()

	var src = bytes.NewReader(secret_src)

	var dst bytes.Buffer

	var length, err = Decrypt(blockSize, crc, rng, src, &dst)

	if err != nil {
		t.Fatal("raise error", err.Error())
	}

	if length != uint64(len(data_src)) {
		t.Fatal("wrong length", length)
	}

	if !bytes.Equal(data_src, dst.Bytes()) {
		t.Fatal("unmatch dst", dst)
	}
}

func TestEncryptWithRand(t *testing.T) {

	var blockSize int = MinBlockSize

	var seed int64 = 123456789

	var data_src = []uint8{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}

	var secret_src = []uint8{
		0xa2, 0xfc, 0x45, 0x90, 0x64, 0x80, 0x77, 0x46, 0x3f, 0x7e,
		0x1d, 0x7c, 0x64, 0xfe, 0x5c, 0x98, 0x7a, 0x00, 0x79, 0xa8,
		0x64, 0xf2, 0x7d, 0xc1, 0xe3, 0x66, 0x31, 0x31, 0x1e, 0x62,
		0xb6, 0x04,
	}

	var rng = &RandWrapper{rand.New(newJavaRandomLikeSource(seed))}

	var crc = crc32.NewIEEE()

	var src = bytes.NewReader(data_src)

	var dst bytes.Buffer

	var length, err = Encrypt(blockSize, crc, rng, src, &dst)

	if err != nil {
		t.Fatal("raise error", err.Error())
	}

	if length != uint64(len(secret_src)) {
		t.Fatal("wrong length", length)
	}

	if !bytes.Equal(secret_src, dst.Bytes()) {
		t.Fatal("unmatch dst", dst)
	}
}

func TestDecryptWithIntRNG(t *testing.T) {

	var blockSize int = MinBlockSize

	var seed int64 = 123456789

	var data_src = []uint8{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}

	var secret_src = []uint8{
		0xa2, 0xfc, 0x45, 0x90, 0x64, 0x80, 0x77, 0x46, 0x3f, 0x7e,
		0x1d, 0x7c, 0x64, 0xfe, 0x5c, 0x98, 0x7a, 0x00, 0x79, 0xa8,
		0x64, 0xf2, 0x7d, 0xc1, 0xe3, 0x66, 0x31, 0x31, 0x1e, 0x62,
		0xb6, 0x04,
	}

	var rng = newJavaRandom(seed)

	var crc = crc32.NewIEEE()

	var src = bytes.NewReader(secret_src)

	var dst bytes.Buffer

	var length, err = Decrypt(blockSize, crc, rng, src, &dst)

	if err != nil {
		t.Fatal("raise error", err.Error())
	}

	if length != uint64(len(data_src)) {
		t.Fatal("wrong length", length)
	}

	if !bytes.Equal(data_src, dst.Bytes()) {
		t.Fatal("unmatch dst", dst)
	}
}

func TestEncryptWithIntRNG(t *testing.T) {

	var blockSize int = MinBlockSize

	var seed int64 = 123456789

	var data_src = []uint8{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}

	var secret_src = []uint8{
		0xa2, 0xfc, 0x45, 0x90, 0x64, 0x80, 0x77, 0x46, 0x3f, 0x7e,
		0x1d, 0x7c, 0x64, 0xfe, 0x5c, 0x98, 0x7a, 0x00, 0x79, 0xa8,
		0x64, 0xf2, 0x7d, 0xc1, 0xe3, 0x66, 0x31, 0x31, 0x1e, 0x62,
		0xb6, 0x04,
	}

	var rng = newJavaRandom(seed)

	var crc = crc32.NewIEEE()

	var src = bytes.NewReader(data_src)

	var dst bytes.Buffer

	var length, err = Encrypt(blockSize, crc, rng, src, &dst)

	if err != nil {
		t.Fatal("raise error", err.Error())
	}

	if length != uint64(len(secret_src)) {
		t.Fatal("wrong length", length)
	}

	if !bytes.Equal(secret_src, dst.Bytes()) {
		t.Fatal("unmatch dst", dst)
	}
}
